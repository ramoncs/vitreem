#include "StdLib.h"
#include "Simulator.h"
#include "JSON.h"
#include "Utils.h"
#include "PIGraphGen.h"
#include "FTGraphGen.h"
#include "FMGraphGen.h"
#include "RingGraphGen.h"

wstring readfile(string filename){
    wfstream wif(filename);
    wstringstream wss;
    wss << wif.rdbuf();
    return wss.str();
}

PII read_pii(JSONArray jarray){
    assert(jarray.size() == 2);
    int a = jarray[0]->AsNumber();
    int b = jarray[1]->AsNumber();
    return PII(a, b);
}

FTGraphGen::Layer read_ftgraphgen_layer(JSONObject jlayer){
    PII n_nodes = read_pii(jlayer[L"n_nodes"]->AsArray());
    PII node_type = read_pii(jlayer[L"node_type"]->AsArray());
    PII node_cap = read_pii(jlayer[L"node_cap"]->AsArray()); 
    PII edge_cap = read_pii(jlayer[L"edge_cap"]->AsArray()); 
    return FTGraphGen::Layer{n_nodes, node_type, node_cap, edge_cap};
}

FTGraphGen* read_ftgraphgen(JSONObject jgg){
    PII n_layers = read_pii(jgg[L"n_layers"]->AsArray()); 
    JSONArray jlayers = jgg[L"layers"]->AsArray();
    vector<FTGraphGen::Layer> layers;
    for(int i=0; i<(int)jlayers.size(); i++){
        layers.push_back(read_ftgraphgen_layer(jlayers[i]->AsObject()));
    }
    return new FTGraphGen(FTGraphGen::Config(n_layers, layers));
}


PIGraphGen* read_pigraphgen(JSONObject jgg){
    int n_dc = jgg[L"n_dc"]->AsNumber();
    JSONObject jdc = jgg[L"datacenter"]->AsObject();
    PIGraphGen::Datacenter dc;
    dc.core_node_cap = jdc[L"core_node_cap"]->AsNumber();
    dc.core_edge_cap = jdc[L"core_edge_cap"]->AsNumber();
    dc.agregation_node_cap = jdc[L"agregation_node_cap"]->AsNumber();
    dc.agregation_edge_cap = jdc[L"agregation_edge_cap"]->AsNumber();
    dc.tor_node_cap = jdc[L"tor_node_cap"]->AsNumber();
    dc.tor_edge_cap = jdc[L"tor_edge_cap"]->AsNumber();
    dc.server_node_cap = jdc[L"server_node_cap"]->AsNumber();
    return new PIGraphGen(PIGraphGen::Config(n_dc, dc));
}

FMGraphGen* read_fmgraphgen(JSONObject jgg){
    PII n_nodes = read_pii(jgg[L"n_nodes"]->AsArray());
    PII node_cap = read_pii(jgg[L"node_cap"]->AsArray()); 
    PII edge_cap = read_pii(jgg[L"edge_cap"]->AsArray()); 
    return new FMGraphGen(FMGraphGen::Config(n_nodes, node_cap, edge_cap)); 
}

RingGraphGen* read_ringgraphgen(JSONObject jgg){
    PII n_nodes = read_pii(jgg[L"n_nodes"]->AsArray());
    PII node_cap = read_pii(jgg[L"node_cap"]->AsArray()); 
    PII edge_cap = read_pii(jgg[L"edge_cap"]->AsArray()); 
    return new RingGraphGen(RingGraphGen::Config(n_nodes, node_cap, edge_cap)); 
}

GraphGen* read_graphgen(JSONObject jgg){
    if(jgg[L"type"]->AsString() == L"PI"){
        return (GraphGen*)read_pigraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"FM"){
        return (GraphGen*)read_fmgraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"FT"){
        return (GraphGen*)read_ftgraphgen(jgg);
    }else if(jgg[L"type"]->AsString() == L"RING"){
        return (GraphGen*)read_ringgraphgen(jgg);
    }else{
        cout << "Undefined type" << endl;
        exit(EXIT_FAILURE);
    }
    return NULL;
}

void test(){
    vector<Node> pnodes = {
        Node(0, 1, 10),
        Node(1, 1, 10),
        Node(2, 1, 10),
        Node(3, 1, 10),
        Node(4, 1, 10),
        Node(5, 1, 10),
        Node(6, 1, 10),
        Node(7, 0, 10),
        Node(8, 0, 10),
        Node(9, 0, 10),
        Node(10, 0, 10),
        Node(11, 0, 10),
        Node(12, 0, 10),
        Node(13, 0, 10),
        Node(14, 0, 10),
    };

    vector<Edge> pedges = {
        Edge(0, 1, 10),
        Edge(0, 2, 10),
        Edge(1, 3, 10),
        Edge(1, 4, 10),
        Edge(2, 5, 10),
        Edge(2, 6, 10),
        Edge(3, 7, 10),
        Edge(3, 8, 10),
        Edge(4, 9, 10),
        Edge(4, 10, 10),
        Edge(5, 11, 10),
        Edge(5, 12, 10),
        Edge(6, 13, 10),
        Edge(6, 14, 10),
    };

    Graph phy(pnodes, pedges);
    Tree tphy(phy, Tree::Converter::PHYSICAL, Tree::Root::LRC);

    cout << tphy.get_box(0,0) << endl;

    vector<Node> vnodes = {
        Node(0, 1, 10),
        Node(1, 0, 10),
        Node(2, 1, 10),
        Node(3, 0, 10),
        Node(4, 1, 10),
        Node(5, 1, 10),
        Node(6, 1, 10),
        Node(7, 1, 10),
    };

    vector<Edge> vedges = {
        Edge(0, 1, 1),
        Edge(1, 2, 1),
        Edge(2, 3, 1),
        Edge(3, 4, 1),
        Edge(4, 5, 1),
        Edge(5, 6, 1),
        Edge(6, 7, 1),
        Edge(7, 0, 1),
    };

    Graph vir(vnodes, vedges);
    Tree tvir(vir, Tree::Converter::VIRTUAL, Tree::Root::LRC);

    //VITreeM mapp(tphy, tvir, VITreeM::Selection::BEST_FIT, 0.05);
    //cout << mapp;


}

int main(int argc, char **argv){
    //test();
    //return 0;

    srand(time(NULL));
    if(argc < 3){
        cout << "usage: simulator <parameters file> <output filename>";
        return -1;
    }

    JSONValue *vroot = JSON::Parse(readfile(argv[1]).c_str());
    if(vroot == NULL){
        cout << "Error to parse " << argv[1] << endl;
        return -1;
    }

    JSONObject root = vroot->AsObject();

    Simulator sim;
    int n_vis = root[L"n_vis"]->AsNumber();
    PII interval = read_pii(root[L"interval"]->AsArray());
    GraphGen *pigen = read_graphgen(root[L"pigraphgen"]->AsObject());
    GraphGen *vigen = read_graphgen(root[L"vigraphgen"]->AsObject());

    double tini = stime();
    sim.run(pigen, vigen, n_vis, interval, argv[2]);
    double tend = stime();

    cout << "\nSimulation time: " << tend - tini << "s\n";

    return 0;
}

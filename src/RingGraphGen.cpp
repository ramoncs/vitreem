#include "RingGraphGen.h"

RingGraphGen::RingGraphGen(Config config):
    config(config)
{}

Graph RingGraphGen::generate(){
    vector<Node> nodes;
    vector<Edge> edges;

    int n_nodes = rand_btw(config.n_nodes);

    for(int u=0; u<n_nodes; u++)
        nodes.push_back(Node(u, u%2, rand_btw(config.node_cap)));

    for(int u=0; u<n_nodes; u++){
        int v = (u+1)%n_nodes;
        edges.push_back(Edge(u, v, rand_btw(config.edge_cap)));
    }

    return Graph(nodes, edges);
}

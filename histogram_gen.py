#!/usr/bin/env python

import matplotlib.pyplot as pyplot
from matplotlib.lines import Line2D
import json
import sys
import math

markers = ['.', 'o', '^', '1', 's', 'p', '*', '+', 'x', 'D', 'h', ',']

colors = [
	(0,255,0),
	(0,0,255),
	(255,0,0),
	(255,0,255),

	(10,255,0),
	(0,0,255),
	(255,255,0),
	(255, 100, 0), 
	(255,0,255),
	(192,192,192),
	(45, 100, 45),
	(128,0,0),
	(128,128,0),
	(20, 100, 245),
	(128,0,128),
	(0,128,128)
]

colors = [(1.0*r/255, 1.0*g/255, 1.0*b/255) for r, g, b in colors]


def graphics_gen(jgraphic, filename):
	pyplot.figure(figsize=(20,15))

	bar_width = 0.35
	opacity = 0.8
	error_config = {'ecolor': '0.3'}

	description = jgraphic["description"]
	pyplot.suptitle(description)

	nplots = len(jgraphic["subgraphics"])
	curplot = 1
	for subgraphic in jgraphic["subgraphics"]:
		if "ver" in subgraphic["title"]:
			continue

		ax = pyplot.subplot(nplots//2 + nplots%2, 2, curplot)
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width*0.8, box.height])

		cnt = 0
		for i, curve in enumerate(subgraphic["curves"]):
			name = curve["name"]
			if r"%" in name: 
				continue
			name = name[:-8]
			cnt += 1
			ys = [y for x, y in curve["points"]]
			n = len(ys)
			mean = sum(ys)/ n
			std = math.sqrt(sum((x-mean)**2 for x in ys)/n)

			bar = pyplot.bar([cnt], [mean], bar_width,
				alpha=opacity,
				color=colors[cnt-1],
				yerr=[std],
				error_kw=error_config,
				label=name)

		pyplot.setp(ax.get_xticklabels(), visible=False)
		pyplot.title(subgraphic["title"])
		pyplot.ylabel(subgraphic["ylabel"])
		pyplot.xlim(xmin=0, xmax=cnt+1)
		if "axis" in subgraphic:
			pyplot.ylim(subgraphic["axis"][2:])
		pyplot.legend(loc="upper left", fontsize=10)
		curplot += 1

	pyplot.savefig(filename)

if __name__ == "__main__":
	if len(sys.argv) < 3:
            print 'usage graphics_gen <infilename> <outfilename>'
        else:
            infilename = sys.argv[1]
            outfilename = sys.argv[2] 
            with open(infilename,'r') as f:
                gplot = json.loads(f.read())
                graphics_gen(gplot, outfilename)
